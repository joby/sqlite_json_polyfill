# php_sqlite_json

A shim tool to make implementations of MySQL JSON functions available in SQLite, although performance will necessarily be limited. Supports shimming connections from PDO or SQLite3 objects, using either PDO::sqliteCreateFunction or SQLite3::createFunction under the hood.