<?php

namespace Joby\SqliteJsonPolyfill\Providers;

use Joby\SqliteJsonPolyfill\PolyfillProvider;

/**
 * @ref https://dev.mysql.com/doc/refman/8.0/en/json-modification-functions.html
 */
class ModificationProvider implements PolyfillProvider
{
}
