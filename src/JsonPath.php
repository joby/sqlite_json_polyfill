<?php

namespace Joby\SqliteJsonPolyfill;

use InvalidArgumentException;
use Joby\SqliteJsonPolyfill\Legs;

/**
 * Parses a JSON path as described in https://dev.mysql.com/doc/refman/8.0/en/json.html#json-path-syntax
 */
class JsonPath
{
    protected array $legs = [];

    public function __construct(string $path)
    {
        $this->legs =
            $legs = array_map(
                static::parseLeg(...),
                static::parsePath($path)
            );
    }

    public function legs(): array
    {
        return $this->legs;
    }

    /**
     * Parses a JSON path into an array of legs. Any legs that were quoted
     * literals are returned as Literal leg objects, and the rest are returned
     * as strings. This is necessary because quoted literals will always be
     * treated just as strings, while others might contain more specific syntax.
     * 
     * @throws InvalidArgumentException 
     * @return array<Legs\Literal|string>
     */
    public static function parsePath($path): array
    {
        $legs = [];
        $leg = '';
        $literal_open = false;
        $literal = false;
        $escape_open = false;
        $escape_unicode = false;
        for ($i = 0; $i < strlen($path); $i++) {
            $char = $path[$i];
            // we are starting a word
            if (!$leg) {
                // reset state
                $literal_open = false;
                $literal = false;
                $escape_open = false;
                $escape_unicode = false;
                // check if we're starting a literal
                if ($char == '"' || $char == "'") {
                    // if we are, record what character we're using
                    $literal_open = $char;
                    continue;
                } else {
                    // if we're not, start the word
                    $leg .= $char;
                }
            } else {
                // we are in the middle of a word
                if ($literal_open) {
                    // we are inside a literal
                    if ($escape_open) {
                        // we're in the middle of an escape sequence
                        if ($char == 'u') {
                            // start reading a unicode escape sequence
                            $escape_unicode = '';
                        } elseif ($escape_unicode !== false) {
                            // if we're in a unicode escape sequence, either add to the sequence or finish and parse it
                            if (strlen($escape_unicode)  == 4) {
                                $leg .= chr(hexdec($escape_unicode));
                                $escape_open = false;
                                $escape_unicode = false;
                            } else {
                                // validate that character is a valid hex character
                                if (!in_array($char, ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'])) {
                                    throw new \InvalidArgumentException('Invalid unicode escape sequence in JSON path: ' . $path);
                                }
                                $escape_unicode .= $char;
                            }
                        } else {
                            // if we're not in a unicode escape sequence, just add the escaped character
                            $leg .= $char;
                            $escape_open = false;
                        }
                    } elseif ($char == '\\') {
                        // start an escape sequence
                        $escape_open = true;
                    } elseif ($char == $literal_open) {
                        // we're at the end of a literal, close it
                        $literal_open = false;
                        // look ahead to check that we're either at the end or have a period next
                        if (isset($path[$i + 1]) && $path[$i + 1] != '.') {
                            throw new \InvalidArgumentException('Invalid JSON path: ' . $path);
                        }
                    }
                } else {
                    // we are not in a literal
                    if ($char == '.') {
                        // we are at the end of a leg, add it to the list and reset
                        if ($literal) $leg = new Legs\Literal($leg);
                        $legs[] = $leg;
                        $leg = '';
                        $literal = false;
                    } else {
                        // we are in the middle of a regular leg
                        // validate that character isn't something invalid
                        if (in_array($char, ['"', "'"])) {
                            throw new \InvalidArgumentException('Invalid JSON path: ' . $path);
                        }
                        $leg .= $char;
                    }
                }
            }
        }
        // return legs
        return $legs;
    }

    /**
     * Parses a JSON path leg string into an appropriate leg object, depending
     * on the string. Because parsePath returns literals as Literal objects,
     * this method also accepts those, and will return them unchanged.
     * 
     * @throws InvalidArgumentException 
     */
    public static function parseLeg(string|Legs\Literal $leg): JsonPathLeg
    {
        if ($leg instanceof Legs\Literal) {
            return $leg;
        } elseif ($leg == '$') {
            return new Legs\Root;
        } elseif ($leg == '*') {
            return new Legs\AllObjectValues;
        } elseif (str_contains($leg, '**')) {
            $parts = explode('**', $leg);
            if (count($parts) != 2) {
                throw new \InvalidArgumentException('Invalid JSON path leg: ' . $leg);
            }
            return new Legs\Wildcard($parts[1], $parts[0]);
        } elseif (str_starts_with($leg, '[') && str_ends_with($leg, ']')) {
            $value = substr($leg, 1, -1);
            if ($value == '*') return new Legs\AllArrayValues;
            $int = intval($value);
            if ($int != $value) {
                throw new \InvalidArgumentException('Invalid JSON path leg: ' . $leg);
            }
            if ($int < 0) {
                throw new \InvalidArgumentException('Invalid JSON path leg: ' . $leg);
            }
            return new Legs\ArrayIndex($int);
        } else {
            throw new \InvalidArgumentException('Invalid JSON path leg: ' . $leg);
        }
    }
}
