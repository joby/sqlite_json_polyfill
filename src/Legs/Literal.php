<?php

namespace Joby\SqliteJsonPolyfill\Legs;

class Literal
{
    public function __construct(protected string $literal)
    {
        if (strlen($literal) === 0) {
            throw new \InvalidArgumentException('Literal leg must not be empty');
        }
    }

    public function literal(): string
    {
        return $this->literal;
    }
}
